# Clase del 04/11
#s = [3,2,4,5]
#len(s) y sale 4
#t = (3,2,4,5)
#len(t)
#s[0] y sale 3
#type(s) y sale <class 'list'>
#type(t) y sale <class 'tuple'>
#s[0:2] desde el o hasta el 2 sin incluir el dos (recordamos q 0 es el primero, o se que 2 sería el tercero)
#r = s[0:2]; luego ponemos type r y saldría <class 'list'>
#len(r) y saldría 2
#Las tuplas son inmutables (no se pueden modificar), en cambio las listas son  mutables(se pueden modificar)
m=42
print(m)
print(type(m))


# Clase 18/11
d = {
    'LHY': 'Roberto',
    'HRT': 'Juan',
    'EER': 'María'
}

print(d)

# Clase 25/11
c = 'cuatro'

s = ('uno', 'dos', 'tres', 'cuatro')

if c in s and len(c) >= 3:
    print('si')

# Clase 02/12
def sum_list(start: int, end: int) -> int:
    """
    returns the sum of integers between start and end.
    :param start: first integer
    :param end: last integer
    :return: the sum
    """
    new_list = list(range(start, end + 1))
    s = sum(new_list)
    return s


a = sum_list(1, 3)
print(a)


# Lo mismo q arriba pero cambiando la primera linea (int lo cambio por list)
# Y cambiar el return s por retur sum_list
def sum_list(start: int, end: int) -> list:
    """
    returns the sum of integers between start and end.
    :param start: first integer
    :param end: last integer
    :return: the sum
    """
    new_list = list(range(start, end + 1))
    return new_list


a = sum_list(1, 3)
print(a)


# Otra cosa
def sum_list(start: int, end: int = 10) -> int:
    """
    returns the sum of integers between start and end.
    :param start: first integer
    :param end: last integer
    :return: the sum
    """
    new_list = list(range(start, end + 1))
    s = sum(new_list)
    return s


a = sum_list(1)
print(a)


def my_sum(*args):
    result = 0
    for x in args:
        result += x
    return result


my_sum(1, 2, 3)

list1 = [1, 2, 3]
list2 = [4, 5]


def sum_product(*args) -> int:
    """
    Returns x_1 + 2 * sum(x_i)

    :param args: x_1
    :return:
    """
    x = args[0] + 2 * sum(args[1:])
    return x

#Otro

def named(**kwargs):
    for key in kwargs():
        print('arg:', key, 'has value:', kwargs[key])


# Clase 09/12
#En vez de usar result = result + value se puede poner result=+valeu


def expon(a, *args):
    return a ** sum(args)

print(expon(10, 2, 3, 1))
#Tambien se puede poner  def expon(a, **kwargs) y luego sum(kwargs)

def print_kwargs(**kwargs):
    print(kwargs['a'])

print_kwargs(a = 1, b = 2)


student_tuples = [('John', 'A', 15),
                  ('Jane', 'B', 12),
                  ('Dave', 'B', 10)]
#student_tuples[1][2] el resultado seria 12 pq nos pide la tupla 1 (0-1-2) y el elemento 2 (0-1-2) vendría a ser el 12
#En total es una lista pero los pequeños son tuplas

def sorting_criteria(x: tuple[str, str, int]) -> int:
    return x [2]

sorting_criteria(student_tuples[0])
#Nos devuelve 15 pq utilizamos la primera tupla (0) y el tercer elemento (2)

#Lo de abajo no se q es
sorted({3,1,5}), reverse = True

#Para ordenar por nombre
sorted(student_tuples, key=sorting_criteria)

#Lo de abajo no se q es
def sorting_criteria(x: tuple[str, str, int]) -> str:
    return x [0]

from math import exp

def g(f, x):
    return exp(f(x))

def f(x):
    return x**2

g(f,3)
#El resultado seria e^(3^(2))

def g(f, x):
    return exp(f(x))

g(lambda x: x**2, 3)

#Ejercicio para ver cosas de debug1277
z = 3
def print_string(z)
    print{'The value is {z}'}
    return z

y = print_string(Z)


# Clase 16/12
def sum_product(my_list: list) -> int:
    x = my_list[0] + 2 * sum(my_list[1:])
    return x


my_list = [3, 3, 4, 5]
print(sum_product(my_list))


# Clase 23/12
age = 19
f'I am {age} years old'
#Saldría i am 19 years old

